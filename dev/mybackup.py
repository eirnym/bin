#!/usr/bin/env python

""" Simple SQL backups."""
import argparse
import getpass
import re
import sys
import urllib.parse

import pymysql


def parse_dsn(dsn: str):
    p = urllib.parse.urlparse(dsn)

    if len(p.path) == 1:
        raise ValueError('database name should be in DSN')

    if p.path.count('/') > 1:
        raise ValueError('database name should not contain / char')

    database = p.path[1:]

    return {
        'host': p.hostname,
        'port': p.port or 3306,
        'user': p.username,
        'passwd': p.password,
        'db': database,
    }


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dsn', help='SQL dsn to use')
    parser.add_argument(
        '-u', '--user', default=None, help='Override user to use')
    parser.add_argument(
        '-p', '--password', default=None, help='Override password to use')
    parser.add_argument(
        '--no-data', action='store_true', help='don\'t save data')
    return parser.parse_args(args)


def obtain_connection_options(dsn, user, password):
    dsn_info = parse_dsn(dsn)

    if not dsn_info['user'] and not user:
        dsn_info['user'] = getpass.getuser()

    if not dsn_info['passwd'] and not password:
        dsn_info['passwd'] = getpass.getpass('password:')

    return dsn_info


def exec_plain(query, cursor):
    cursor.execute(query)
    return cursor.fetchall()


def exec_dict(query, cursor):  # naming sucks
    data = exec_plain(query, cursor)
    fields = list(map(lambda x: x[0], cursor.description))
    return [dict(zip(fields, row)) for row in data]


def get_tables(cursor):
    data = exec_plain('SHOW TABLES', cursor)
    return map(lambda row: row[0], data)


def get_creation(table, cursor):
    """Returns SQL create statement for a table/view"""
    # what could go wrong here? ;-P

    return exec_dict(f'SHOW CREATE TABLE `{table}`', cursor)[0]


def read_tables_info(no_data, cursor):
    tables = get_tables(cursor)
    table_info = {}
    for table_name in tables:
        creation = get_creation(table_name, cursor)
        if 'View' in creation:
            kind = 'view'
            creation_key = 'Create View'
        elif 'Table' in creation:
            kind = 'table'
            creation_key = 'Create Table'
        else:
            print(f'Unknown object: {creation!r}', file=sys.stderr)
            continue

        table_info[table_name] = {
            'kind': kind,
            'creation': creation,
            'SQL': fix_ddl(creation[creation_key]).strip(),
            #!! read generator before cursor is gone !!#
            'data': get_table_data(table_name, kind, cursor, no_data)
        }
    return table_info


drop_autoincrement_re = re.compile(
    r'(?<=ENGINE=InnoDB )(AUTO_INCREMENT=[0-9]* )(?=.*)', re.M|re.S)


def fix_ddl(ddl):
    if 'CREATE TABLE' in ddl:
        return drop_autoincrement_re.sub('', ddl)
    else:
        return ddl


def get_data(table, cursor):
    """Reads data from a table"""
    return exec_dict(f'SELECT * FROM `{table}`', cursor)


def create_insert_template(table, names):
    if len(names) == 0:
        raise ValueError('WTF?')

    sql_names = ', '.join(map(lambda name: f"`{name}`", names))
    sql_params = ', '.join(map(lambda name: f"%({name})s", names))

    return f'INSERT INTO `{table}` ({sql_names}) VALUES ({sql_params});'


def mogrify(table, data, cursor):
    query = create_insert_template(table, data)
#    {key: conn.literal(val) for (key, val) in args.items()}
    return cursor.mogrify(query, data)


def get_table_data(table, kind, cursor, no_data):
    if kind != 'table' or no_data:
        return

    data = get_data(table, cursor)
    yield from map(lambda row: mogrify(table, row, cursor), data)


def output_schema(table_info):
    for table_name in sorted(table_info):
        info = table_info[table_name]
        kind = info['kind'].upper()
        SQL = info['SQL']
        print(f'\n-- CREATE {kind} {table_name}\n{SQL};')


def output_data(no_data, table_info):
    if no_data:
        return

    for table_name in sorted(table_info):
        info = table_info[table_name]
        statements = '\n'.join(info['data'])
        if not statements:
            continue

        print(f'\n-- Data for: {table_name}\n{statements}')


def main():
    args = parse_args()
    conn_params = obtain_connection_options(args.dsn, args.user, args.password)

    with pymysql.connect(**conn_params) as cursor:
        #cursor = conn.cursor()
        table_info = read_tables_info(args.no_data, cursor)
        output_schema(table_info)
        output_data(args.no_data, table_info)


if __name__ == '__main__':
    main()
