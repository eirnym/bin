#!/usr/bin/env python3

import argparse
import base64
import sys
import secrets


def parse_args(args=None):
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        '-p', '--printable', action='store_true',
        help='Printable random characters')
    group.add_argument(
        '-a', '--alphanumeric', action='store_true',
        help='Alphanumeric random characters')
    group.add_argument(
        '-n', '--numeric', action='store_true',
        help='numeric random characters')
    group.add_argument(
        '-l', '--letter', action='store_true',
        help='Alphabetic random characters')
    group.add_argument(
        '-s', '--special', action='store_true',
        help='Special printable random characters')
    group.add_argument(
        '-b', '--bytes', action='store_true',
        help='Random bytes')

    parser.add_argument(
        '-B', '--base64', action='store_true',
        help='Output is a base64'
    )
    parser.add_argument(
        '--amount', type=int,
        default=1, help='Amount of generated lines')
    parser.add_argument(
        'length', type=int, default=64, nargs='?',
        help='length of random sequence')
    return parser.parse_args(args)


def random_bytes(length):
    return secrets.token_bytes(length)


def random_characters(length, predicate):
    """Transform random bytes to random ASCII chars by """
    result = ''

    while len(result) < length:
        rnd = bytes(map(lambda x: x & 0x7f, secrets.token_bytes(length*2)))
        result += ''.join(filter(predicate, rnd.decode('ascii')))

    return result[:length]


def printable_predicate(c):
    return c.isprintable() and not c.isspace()


def alphanumeric_predicate(c):
    return c.isalnum()


def number_predicate(c):
    return c.isdigit()


def letter_predicate(c):
    return c.isalpha()


def special_predicate(c):
    return c.isprintable() and not c.isspace() and not c.isalnum()


def select_predicate(args):
    if args.printable:
       predicate = printable_predicate
    elif args.alphanumeric:
        predicate = alphanumeric_predicate
    elif args.numeric:
        predicate = number_predicate
    elif args.special:
        predicate = special_predicate
    elif args.letter:
        predicate = letter_predicate
    else:
        raise ValueError("Predicate is unknown")

    return predicate


def get_data(args, predicate):
    if args.bytes:
        data = secrets.token_bytes(args.length)
        if args.base64:
            return base64.b64encode(data).decode('utf-8')
        return data

    data = random_characters(args.length, predicate)
    if args.base64:
        return base64.b64encode(data.encode('utf-8')).decode('utf-8')
    return data


def main(args):
    if args.bytes:
        predicate = None
    else:
        predicate = select_predicate(args)

    for i in range(args.amount):
        data = get_data(args, predicate)
        if isinstance(data, bytes):
            sys.stdout.buffer.write(data)
        else:
            print(data)


if __name__ == '__main__':
    main(parse_args())
